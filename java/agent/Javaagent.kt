package agent

import com.sun.tools.attach.VirtualMachine
import skullproject.asm.InjectWrapper
import skullproject.reborn.Reborn
import skullproject.reborn.events.EventManager
import skullproject.reborn.events.Initialization
import skullproject.utils.io.Logger
import java.io.File
import java.lang.instrument.Instrumentation
import java.util.*


/**
 * [Anticheat] Created by TheDestinyPig
 * Class made on 02/06/2017 10:45
 */

class Javaagent {

    companion object {

        /**
         * @param message
         *
         * @throws AgentException
         */
        fun fail(message: String): Nothing = throw AgentException(message)

        /**
         * Loads javaagent into a minecraft running ^.^
         */
        fun loadAgent(): Unit {

            var jarFile: File? = null
            var vm: VirtualMachine? = null

            Logger.info("Fetching file path...")
            try {
                jarFile = File(Bootstrap::class.java.protectionDomain.codeSource.location.toURI().path)
            } catch(e:Exception) {
                fail("Failed getting filepath.")
            }


            Logger.info("Binding attachment library...")
            try {
                Class.forName("com.sun.tools.attach.VirtualMachine.")
            } catch(e:Exception) {
                fail("Failed loading com.sun.tools attachment library. :(")
            }

            Logger.info("Loading attachment library...")
            try {
                System.loadLibrary("attach")
            } catch(e:Exception) {
                fail("Failed loading com.sun.tools attachment library. :(")
            }


            Logger.info("Fetching Minecrafts process id...")
            val reader = Scanner(System.`in`)

            Logger.info("Attempting to attach into Minecraft...")
            try {
                vm = VirtualMachine.attach(reader.next())
            } catch(e:Exception) {
                fail("Failed loading into Minecraft. :(")
            }

            Logger.info("Injecting the modifications...")
            try {
                vm.loadAgent(jarFile.absolutePath)
            } catch(e:Exception) {
                fail("Failed to load the modifications. :(")
            }

            Logger.info("Detaching...")
            try {
                vm.detach()
            } catch(e:Exception) {
                fail("Could not detach javaagent. :(")
            }
            Logger.info("Sucessfully injected Skull Project.")

        }

        /**
         * Agentmain used to inject events after runtime.
         *
         * @param args
         * *
         * @param instrumentation
         */
        @JvmStatic fun agentmain(args: String, instrumentation: Instrumentation) {

            // Adds the events to minecrafts classes
            EventManager.initEvents()
            EventManager.getEvents().forEach { s -> InjectWrapper.addInjection(s.javaClass) }
            // Starts injection into minecraft
            InjectWrapper.startInjection(instrumentation)
            // Have to start them now as Minecraft has already launched
            Initialization.preInit(Reborn.reborn)
            Initialization.postInit(Reborn.minecraft, Reborn.reborn)
        }
    }

}