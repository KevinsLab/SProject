package agent

import skullproject.asm.InjectWrapper
import skullproject.reborn.events.EventManager
import java.lang.instrument.Instrumentation

/**
 * [Anticheat] Created by TheDestinyPig
 * Class made on 03/05/2017 17:58
 */
class Bootstrap {

    companion object {
        /**
         * Premain used to inject events before runtime.
         *
         * @param args
         * *
         * @param instrumentation
         */
        @JvmStatic fun premain(args: String?, instrumentation: Instrumentation) {

            EventManager.initEvents()
            EventManager.getEvents().forEach { s -> InjectWrapper.addInjection(s.javaClass) }
            InjectWrapper.startInjection(instrumentation)
        }

        /**
         * Probaly should be an installer but for now it just loads the javaagent :)
         *
         * @param args
         */
        @JvmStatic
        fun main(args: Array<String>) {

        }

    }

}
