package skullproject.utils.io

import skullproject.utils.misc.SystemUtil
import java.io.File
import java.util.*
import java.util.regex.Matcher



object FileUtil {

    fun bindDir(vararg subDir: String): File {

        val path = Arrays.toString(subDir)
                .replace("[", "")
                .replace("]", "")
                .replace(", ".toRegex(), Matcher.quoteReplacement(SystemUtil.getFileSeparator()))
        return File(path)
    }


}
