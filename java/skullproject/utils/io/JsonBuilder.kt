package skullproject.utils.io

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject

/**
 * [Anticheat] Created by TheDestinyPig
 * Class made on 08/06/2017 17:46
 */
class JsonBuilder internal constructor() {

    var json: JsonObject
        internal set

    init {
        json = JsonObject()
    }

    fun addObject(property: String, value: String): JsonBuilder {

        json.addProperty(property, value)
        return this
    }

    fun addArray(property: String): JsonBuilder {

        val datasets = JsonArray()
        json.add(property, datasets)
        return this
    }

    fun add(property: String, value: JsonElement): JsonBuilder {

        json.add(property, value)
        return this
    }

}
