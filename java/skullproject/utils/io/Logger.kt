package skullproject.utils.io

import org.slf4j.LoggerFactory
import skullproject.reborn.Reborn

/**
 * [Client] Created by TheDestinyPig
 * Class made on 18/04/2017 16:28
 */
class Logger {

    companion object {

        private val logger = LoggerFactory.getLogger(Reborn::class.java)

        /* Getters */
        var logs: Int = 0
            private set
        var infos: Int = 0
            private set
        var patchs: Int = 0
            private set
        var errors: Int = 0
            private set
        var warns: Int = 0
            private set
        var debugs: Int = 0
            private set

        @JvmStatic
        private fun log(title: String, line: String) {
            logger.info(line)
        }

        @JvmStatic
        fun info(line: String) {
            logger.info(line)
            infos++
        }

        @JvmStatic
        fun patch(line: String) {
            log("[PATCH] ", line)
            patchs++
        }

        @JvmStatic
        fun debug(line: String) {
            logger.debug(line)
            debugs++
        }

        @JvmStatic
        fun error(line: String) {
            logger.error(line)
            errors++
        }

        @JvmStatic
        fun warn(line: String) {
            logger.warn(line)
            warns++
        }

        fun border() {
            info("---------------------------------")
        }

        @JvmStatic
        fun printLogInfo() {

            border()
            when {
                0 < infos -> info("Sysout info: $infos")
                0 < logs -> info("Sysout logs: $logs")
                0 < patchs -> info("Sysout patchs: $patchs")
                0 < errors -> info("Sysout errors: $errors")
                0 < warns -> info("Sysout warnings: $warns")
                0 < debugs -> info("Sysout debugs: $debugs")
            }
            border()
        }
    }
}
