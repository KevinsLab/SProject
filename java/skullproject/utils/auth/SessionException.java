package skullproject.utils.auth;

public class SessionException extends Exception
{
      //Parameterless Constructor
      public SessionException() {}

      //Constructor that accepts a message
      public SessionException(String message)
      {
         super(message);
      }
 }