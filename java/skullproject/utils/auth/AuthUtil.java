package skullproject.utils.auth;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.Proxy;

import com.mojang.authlib.Agent;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;

import net.minecraft.client.Minecraft;
import net.minecraft.util.Session;

public class AuthUtil {

	private YggdrasilUserAuthentication auth;

	private String email;
	private String password;

	public AuthUtil(String email, String password) {

		this.email = email;
		this.password = password;

	}

	public void login() throws AuthenticationException {
		
		auth = (YggdrasilUserAuthentication) new YggdrasilAuthenticationService(Proxy.NO_PROXY, "")
				.createUserAuthentication(Agent.MINECRAFT);
		auth.setUsername(email);
		auth.setPassword(password);
		auth.logIn();
		updateSession();

	}
	
	public boolean canLogIn() {
		
		return auth.canLogIn();
	}
	
	public boolean canPlayOnline() {
		
		return auth.canPlayOnline();
	}
	
	public boolean isLoggedIn() {
		
		return auth.isLoggedIn();
	}
	
	public GameProfile getProfile() {
		
		return auth.getSelectedProfile();
	}
	
	public void logout() {
		
		auth.logOut();;
	}

	private void updateSession() {

		try {
			Field session = Minecraft.class.getDeclaredField("session");
			Field modifiersField = Field.class.getDeclaredField( "modifiers" );
			
			session.setAccessible(true);
            modifiersField.setAccessible( true );
            modifiersField.setInt( session, session.getModifiers() & ~Modifier.FINAL );
			session.set(Minecraft.getMinecraft(), new Session(auth.getSelectedProfile().getName(),
					auth.getSelectedProfile().getId().toString(), auth.getAuthenticatedToken(), "mojang"));
		} catch (Exception e) {
			//TODO: FINISH
			e.printStackTrace();
		}
	}
}
