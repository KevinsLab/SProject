package skullproject.utils.security

import org.apache.commons.codec.DecoderException
import org.apache.commons.codec.binary.Hex.decodeHex
import org.apache.commons.codec.binary.Hex.encodeHex
import skullproject.utils.io.FileUtil
import skullproject.utils.misc.SystemUtil
import java.security.Key
import javax.crypto.spec.SecretKeySpec

object CryptoUtil {

    var location = FileUtil.bindDir(SystemUtil.getHome(), ".skullproject", "key.lock")

    val personalKey: Key?
        @Throws(Exception::class)
        get() {
            if (!location.exists()) {
                val encoded = AES.generateSymmetricKey()
                val hex = encodeHex(encoded.encoded)
                location.writeText(String(hex))
                return encoded
            }

            val data = location.readLines().get(0)

            val encoded: ByteArray

            try {
                encoded = decodeHex(data.toCharArray())
            } catch (e: DecoderException) {
                e.printStackTrace()
                return null
            }

            return SecretKeySpec(encoded, "AES")
        }
}
