package skullproject.utils.misc;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class SystemUtil {

	private final static String OS = System.getProperty("os.name").toLowerCase();
	private final static String OS_VERSION = System.getProperty("os.version").toLowerCase();
	
	private final static String USER_NAME = System.getProperty("user.name");
	private final static String USER_HOME = System.getProperty("user.home");
	private final static String USER_DIR= System.getProperty("user.dir");
	
	private final static String JAVA_VERSION = System.getProperty("java.version").toLowerCase();
	private final static String FILE_SEPARATOR = System.getProperty("file.separator").toLowerCase();
	
	public static String getUsername() {
		
		return USER_NAME;
	}
	
	public static String getDir() {
		
		return USER_DIR;
	}
	
	
	public static String getHome() {
		
		return USER_HOME;
	}
	
	public static String getFileSeparator() {
		
		return FILE_SEPARATOR;
	}
	
	public static String getJavaVersion() {
		
		return JAVA_VERSION;
	}
	
	public static String getOS() {
		
		return OS;
	}
	
	public static String getOSVersion() {
		
		return OS_VERSION;
	}
	
	public static boolean isWindows() {

		return (OS.contains("win"));
	}

	public static boolean isMac() {

		return (OS.contains("mac"));
	}

	public static boolean isUnix() {

		return (OS.contains("nix") || OS.contains("nux") || OS.contains("aix") );
	}

	public static boolean isSolaris() {

		return (OS.contains("sunos"));
	}

	@Retention(RetentionPolicy.RUNTIME)
	public @interface Requirements {

		OSType System() default OSType.ALL;
	    JavaType JAVA() default JavaType.JAVA_SE6;

	    
	    public enum JavaType {
	        JAVA_SE6,
	        JAVA_SE7,
	        JAVA_SE8;
	    }
	    
		public enum OSType {
			ALL(),
			WINDOWS,
			MAC,
			UNIX;
			
		}
   
	}
}
