package skullproject.reborn

import net.minecraft.client.Minecraft
import net.minecraft.client.entity.EntityPlayerSP
import net.minecraft.client.gui.FontRenderer
import net.minecraft.client.multiplayer.WorldClient
import net.minecraft.client.settings.GameSettings
import skullproject.reborn.hud.HUDManager
import skullproject.utils.io.FileUtil
import java.io.File

/**
 * [eclipse] Created by TheDestinyPig
 * Class made on 20/03/2017 17:43
 */
open class Reborn {
    companion object {

        var INSTANCE: Reborn? = null

        @JvmStatic val reborn: Reborn by lazy { Reborn() }

        @JvmStatic val hudManager: HUDManager by lazy { HUDManager() }

        @JvmStatic val minecraft: Minecraft
            get() = Minecraft.getMinecraft()

        @JvmStatic val fontRenderer: FontRenderer
            get() = Minecraft.getMinecraft().fontRendererObj

        @JvmStatic val fileLocation: File
            get() = FileUtil.bindDir(minecraft.mcDataDir.absolutePath, "reborn")

        @JvmStatic val thePlayer: EntityPlayerSP
            get() = Minecraft.getMinecraft().thePlayer

        @JvmStatic val theWorld: WorldClient
            get() = Minecraft.getMinecraft().theWorld

        @JvmStatic val gameSettings: GameSettings
            get() = Minecraft.getMinecraft().gameSettings



    }
}
