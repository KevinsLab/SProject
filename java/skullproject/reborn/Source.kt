package skullproject.reborn

import java.util.*


/**
 * [eclipse] Created by TheDestinyPig
 * Class made on 20/03/2017 17:43
 */
object Source {

    private val client = "Skull Project"
    private val version = "2.0"

    private val developers = arrayOf("TheDestinyPig", "CoalOres")
    private val beta_testers = arrayOf("Simbae", "Flinted")

    var isDebug = false

    val authors: ArrayList<String>
        get() {

            val authors = ArrayList<String>()
            Arrays.stream(developers).sorted().forEach { dev -> authors.add(dev) }
            return authors
        }

    val betaTesters: ArrayList<String>
        get() {

            val testers = ArrayList<String>()
            Arrays.stream(beta_testers).sorted().forEach { tester -> testers.add(tester) }
            return testers
        }

    enum class Author {
        COALORES,
        THEDESTINYPIG
    }
}
