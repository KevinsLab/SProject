package skullproject.reborn.config.json

/**
 * [Anticheat] Created by TheDestinyPig
 * Class made on 10/06/2017 13:47
 */
class Settings(SAVES: Array<Save>, HUDS: Array<HUDType>) {

    val saves: Array<Save> = SAVES
    val huds: Array<HUDType> = HUDS

}