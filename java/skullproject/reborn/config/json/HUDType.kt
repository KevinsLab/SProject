package skullproject.reborn.config.json

/**
 * [Anticheat] Created by TheDestinyPig
 * Class made on 20/05/2017 11:26
 */
public class HUDType internal constructor(val hudname: String, val desc: String, private val xCoord: Save, private val yCoord: Save, val modules: Array<String>) {

    fun getxCoord(): Save {
        return xCoord
    }

    fun getyCoord(): Save {
        return yCoord
    }
}
