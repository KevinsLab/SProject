package skullproject.reborn.config.json

/**
 * [Client] Created by TheDestinyPig
 * Class made on 20/04/2017 20:09
 */
public class Save(val savedName: String, val datatype: Datatype, var value: Any) {

    val valueAsString: String
        get() = this.value.toString()

    val valueAsBool: Boolean
        get() = java.lang.Boolean.valueOf(this.valueAsString)

    val valueAsInt: Int
        get() = java.lang.Integer.valueOf(this.valueAsString.replace(".0", ""))

    val valueAsFloat: Float
        get() = java.lang.Float.valueOf(this.valueAsString)

    public enum class Datatype {
        BOOLEAN,
        STRING,
        FLOAT,
        INTEGER
    }
}
