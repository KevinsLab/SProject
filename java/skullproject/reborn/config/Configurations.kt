package skullproject.reborn.config

import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import skullproject.reborn.Reborn
import skullproject.reborn.config.json.HUDType
import skullproject.reborn.config.json.Save
import skullproject.reborn.config.json.Settings
import skullproject.utils.io.FileUtil
import skullproject.utils.io.Logger
import java.io.File
import java.io.FileNotFoundException
import java.io.FileReader



/**
 * Created by TheDestinyPig on 19/04/2017.
 */
class Configurations {

    init {
        try {
            load()
        } catch(e: FileNotFoundException) {
            Logger.info("Config file not found. Attempting to create one.")
            saveDefaults()
            load()
        }
    }

    companion object {

        val MAIN_HUD = HUDType("MainHUD", "Displays General HUD",
                Save("POS_X", Save.Datatype.INTEGER, 1),
                Save("POS_Y", Save.Datatype.INTEGER, 1), arrayOf("FPS"))

        val SHADOW = Save("SHADOW", Save.Datatype.BOOLEAN, true)

        val location: File = FileUtil.bindDir(Reborn.fileLocation.absolutePath, "config.json")
        val saves = ArrayList<Save>()

        fun save() {
            val file: File = File(location.toURI())
            file.writeText(Gson().toJson(Settings(saves.toTypedArray(), Reborn.hudManager.huds.toTypedArray())))
        }

        fun saveDefaults() {
            val file: File = File(location.toURI())
            val huds: Array<HUDType> = arrayOf(MAIN_HUD)
            val saves: Array<Save> = arrayOf(SHADOW)
            if(!Reborn.fileLocation.exists())
                Reborn.fileLocation.mkdir()
            file.writeText(Gson().toJson(Settings(saves, huds)))
        }

        @Throws(FileNotFoundException::class)
        fun load(){
            val gson = Gson()
            val reader = JsonReader(FileReader(location))
            val data: Settings = gson.fromJson(reader, Settings::class.java)
            for(hud in data.huds)
                Reborn.hudManager.huds.add(hud)
        }

    }
}
