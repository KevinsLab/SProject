package skullproject.reborn.hud.module

import skullproject.reborn.Reborn

/**
 * [Anticheat] Created by TheDestinyPig
 * Class made on 05/06/2017 19:55
 */
class FPSModule: Module{

    override fun value(): String = Reborn.minecraft.debug
}