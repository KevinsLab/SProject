package skullproject.reborn.hud.module

/**
 * [Anticheat] Created by TheDestinyPig
 * Class made on 04/06/2017 19:37
 */
interface Module {

    fun value(): String
}
