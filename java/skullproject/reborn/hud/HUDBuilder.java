package skullproject.reborn.hud;

import net.minecraft.client.Minecraft;

import java.awt.*;

/**
 * [Anticheat] Created by CookingKevin
 * Class made on 18/06/2017 10:53
 */
public class HUDBuilder {

    private String message;

    private int xCoord = 1;

    private int yCoord = 1;

    private int colour = 0xFFFFFF;

    public HUDBuilder setXCoord(int xCoord) {
        this.xCoord = xCoord;
        return this;
    }

    public HUDBuilder setYCoord(int yCoord) {
        this.yCoord = yCoord;
        return this;
    }

    public HUDBuilder addText(String text) {
        this.message += text;
        return this;
    }

    public HUDBuilder setText(String text) {
        this.message = text;
        return this;
    }

    public HUDBuilder setColour(Color colour) {
        this.colour = colour.hashCode();
        return this;
    }

    public void build() {
        System.out.println("Tes");
        Minecraft.getMinecraft().fontRendererObj.drawString(message, xCoord, yCoord, colour);
    }

}
