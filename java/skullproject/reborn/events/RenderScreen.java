package skullproject.reborn.events;

import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.RuntimeType;
import net.bytebuddy.implementation.bind.annotation.SuperCall;
import skullproject.asm.Bind;
import skullproject.utils.io.Logger;

import java.lang.reflect.Method;
import java.util.concurrent.Callable;

/**
 * [Anticheat] Created by TheDestinyPig
 * Class made on 05/06/2017 20:34
 */
@Bind(none = "net.minecraft.client.gui.GuiIngame")
public class RenderScreen extends Event {

    @RuntimeType
    @Bind(none = "renderGameOverlay")
    public static Object init(@Origin Method method, @SuperCall Callable<?> callable) {
        try {

            return callable.call();
        } catch(Exception e) {
            Logger.error("Error during initialization.");
        } finally {
            getHudManager().renderHUDS();
        }
        return null;
    }
}
