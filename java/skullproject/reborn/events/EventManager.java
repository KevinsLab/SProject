package skullproject.reborn.events;

import java.util.ArrayList;

/**
 * [eclipse] Created by TheDestinyPig
 * Class made on 16/04/2017 15:39
 */
public class EventManager {

    private static ArrayList<Event> events = new ArrayList<>();

    public static void initEvents() {

        register(new RenderScreen(),
                new Initialization());
    }

    public static void register(Event... e) {

        for(Event event : e) {
            events.add(event);
        }
    }

    public static ArrayList<Event> getEvents() {

        return events;
    }

}
