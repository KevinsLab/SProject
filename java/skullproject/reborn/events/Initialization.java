package skullproject.reborn.events;

import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.RuntimeType;
import net.bytebuddy.implementation.bind.annotation.SuperCall;
import net.minecraft.client.Minecraft;
import skullproject.asm.Bind;
import skullproject.reborn.Reborn;
import skullproject.reborn.config.Configurations;
import skullproject.utils.io.Logger;

import java.lang.reflect.Method;
import java.util.concurrent.Callable;

import static org.lwjgl.opengl.Display.setTitle;

/**
 * [eclipse] Created by TheDestinyPig
 * Class made on 16/04/2017 15:18
 */

@Bind(none = "net.minecraft.client.Minecraft")
public class Initialization extends Event {



    @RuntimeType
    @Bind(none = "startGame")
    public static Object init(@Origin Method method, @SuperCall Callable<?> callable) {
        try {
            preInit(getReborn());
            return callable.call();
        } catch(Exception e) {
            Logger.error("Error during initialization.");
        } finally {
            postInit(getMinecraft(), getReborn());
        }

        return null;
    }

    public static void preInit(Reborn reborn) {

       // reborn.getHudManager().getModules().initModules();
        Logger.info("Initialization successful.");
    }

    public static void postInit(Minecraft mc, Reborn reborn) {

        new Configurations();
        setTitle("Skull Project - Reborn");
    }


}
