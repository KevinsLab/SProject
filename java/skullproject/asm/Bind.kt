package skullproject.asm

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy


/**
 * Binds an interceptor to Minecrafts Method.
 */
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE, AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(RetentionPolicy.RUNTIME)
annotation class Bind(
        /**
         * None Obfuscated Minecraft Code.
         */
        val none: String = "",
        /**
         * Minecraft's Default Obfuscation Method.
         */
        val notch: String = "",
        /**
         * Forges's Obfuscation Method.
         */
        val searge: String = "")
