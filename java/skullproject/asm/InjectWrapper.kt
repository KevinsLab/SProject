package skullproject.asm

import net.bytebuddy.agent.builder.AgentBuilder
import net.bytebuddy.description.method.MethodDescription
import net.bytebuddy.description.type.TypeDescription
import net.bytebuddy.implementation.MethodDelegation
import net.bytebuddy.matcher.ElementMatchers.*
import skullproject.utils.io.Logger
import java.lang.instrument.Instrumentation
import java.lang.reflect.Method
import java.util.*


/**
 * [Anticheat] Created by TheDestinyPig
 * Class made on 14/05/2017 11:07
 */
class InjectWrapper {
    companion object {
        /**
         * A field keeping the obfuscation type.
         */
        private var mapping: Obfuscation = Obfuscation.NONE

        /**
         * A queue for each class ready to be injected.
         */
        private val injection_queue: Queue<Class<*>> = LinkedList<Class<*>>()

        /**
         * Starts injection process on the classes queued.
         * Removes the injected classes from the queue.
         *
         * @param instrument
         */
        @JvmStatic fun startInjection(instrument: Instrumentation) {

            Logger.info("Starting injection! ")
            injection_queue.forEach { target ->
                val classLocation = if (target.getAnnotation(Bind::class.java) != null) getClassLocation(target) else ""
                for (target_method in target.declaredMethods) {
                    if (target_method.getAnnotation(Bind::class.java) != null) {
                        val methodName = getMethodName(target_method)
                        if (classLocation != null && methodName != null) {
                            addMethodPatch(classLocation, target, methodName).installOn(instrument)
                        }
                    }
                }
            }
            clearInjectionQueue()
            Logger.info("Injection complete! :)")
        }

        /**
         * Gets class location even when obfuscated.
         * @param clazz
         * *
         * @return The annotation supplying the obfuscated name.
         */
        @JvmStatic private fun getClassLocation(clazz: Class<*>): String? {

            val annotation = clazz.getAnnotation(Bind::class.java)
            when (mapping) {
                Obfuscation.NONE -> return annotation.none
                Obfuscation.NOTCH -> return annotation.notch
                Obfuscation.SEARGE -> return annotation.searge
            }
            return null
        }

        /**
         * Gets method location even when obfuscated.
         *
         * @param method
         * *
         * @return The annotation supplying the obfuscated name.
         */
        @JvmStatic private fun getMethodName(method: Method): String {

            when (mapping) {
                Obfuscation.NONE -> return method.getAnnotation(Bind::class.java).none
                Obfuscation.NOTCH -> return method.getAnnotation(Bind::class.java).notch
                Obfuscation.SEARGE -> return method.getAnnotation(Bind::class.java).searge
            }
            return method.getAnnotation(Bind::class.java).none
        }

        /**
         * Addes classes to the injection queue.
         *
         * @param injects
         */
        @JvmStatic fun addInjection(vararg injects: Class<*>) {

            for (inject in injects) {
                injection_queue.add(inject)
            }
        }

        /**
         * Addes classes to the injection queue.
         *
         * @param injects
         */
        @JvmStatic fun addInjection(injects: ArrayList<Class<*>>) {

            for (inject in injects) {
                injection_queue.add(inject)
            }
        }

        /**
         * Clears the injection queue
         */
        @JvmStatic fun clearInjectionQueue() {

            injection_queue.clear()
        }

        /**
         * A predefined agentbuilder used to intercept methods.
         *
         * @param pkg
         *
         * @param clazz
         *
         * @param method
         *
         * @return
         */
        @JvmStatic fun addMethodPatch(pkg: String, clazz: Class<*>, method: String): AgentBuilder {

            return AgentBuilder.Default()
                    .type(hasSuperType<TypeDescription>(named<TypeDescription>(pkg)))
                    .transform { builder, type, classLoader, module ->
                        builder.method(nameContains<MethodDescription>(method))
                                .intercept(MethodDelegation.to(clazz))
                    }
        }


        /**
         * Returns what injection method minecraft is currently using.
         */
        @JvmStatic fun getObfucationMethod() {
            // TODO: Find a way to find out the current miencraft obfucation method
        }

        /**
         * Sets obfuscation method.
         * @param method
         */
        @JvmStatic fun setObfuscationMethod(method: Obfuscation) {

            mapping = method
        }

        /**
         * Returns if the obfuscation method is set to forge (searge).
         *
         * @return
         */
        @JvmStatic val isForge: Boolean
            get() = mapping == Obfuscation.SEARGE

        /**
         * Returns if the obfuscation method is set to mcp (notch).
         *
         * @return
         */
        @JvmStatic val isMCP: Boolean
            get() = mapping == Obfuscation.NOTCH

        /**
         * Returns if the obfuscation method is set to unobfuscated.
         *
         * @return
         */
        @JvmStatic val isUnobfucated: Boolean
            get() = mapping == Obfuscation.NONE

        /**
         * Obfuscation enums.
         */
        enum class Obfuscation {
            NONE,
            SEARGE,
            NOTCH

        }
    }
}
